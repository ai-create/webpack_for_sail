/*
  Core 
/*---------------------------------------*/
import Vue from 'vue';
import VueRouter from 'vue-router';
/*
  Pages 
/*---------------------------------------*/
import Home from '../vue/home.vue';

/*
  Use
/*---------------------------------------*/
Vue.use(VueRouter);

/*
  Routes
/*---------------------------------------*/
const routes = [
	{path: '/', name: 'home', component: Home},
];

/*
  Export 
/*---------------------------------------*/
export default new VueRouter({
	routes,
})
