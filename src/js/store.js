'use strict';

/*
  Imports 
/*---------------------------------------*/
import Vuex from 'vuex';
import Vue from 'vue';
import * as types from './mutation-types';
// import axios from 'axios';

/*
  use 
/*---------------------------------------*/
Vue.use(Vuex);

/*
  Define 
/*---------------------------------------*/
export default new Vuex.Store({
  /*
    State 
  /*---------------------------------------*/
  state: {
    root: 'http://localhost:3000',
  },


  /*
    Getters 
  /*---------------------------------------*/
  getters: {
    root: state => state.root,
  },


  /*
    Mutations 
  /*---------------------------------------*/
  mutations: {
    [types.CHANGE_MENU](state,menu){
      state.isfood = (menu === 'food')?true:false;
    },
  },

  /*
    Actions 
  /*---------------------------------------*/
  actions: {
    [types.CHANGE_MENU]({commit},menu){
      commit(types.CHANGE_MENU,menu);
    },

  }

});

